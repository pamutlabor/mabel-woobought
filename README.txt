=== WooBought Pro ===
Contributors: maartenbelmans
Tags: woocommerce, ecommerce, notification, recently bought, recent, purchases, recent purchases, alert, recent purchases notification
Requires at least: 3.7
Tested up to: 5.1.1
Stable tag: 1.3.4

Nicely display recent purchases to your visitors with non-intrusive notifications. Add social proof, trust & product visibility to WooCommerce stores.

== Description ==

Beautifully display recent purchases to all your online store visitors with non-intrusive notifications.
A few advantages:

* Let your visitors know your brand is active, alive and popular.
* Create urgency and motivate visitors to buy your products.
* Show that other people are buying and loving your brand. Build credibility.
* Show your visitors products they might not come across by simply browsing your website.

[Find out more here](https://studiowombat.com/plugin/woobought/).

== Installation ==

Installing the plugin is very easy and like most other plugins: [follow these instructions](https://studiowombat.com/kb-article/installing-a-plugin/).

== Changelog ==

= Version 1.3.4 =
 * Fix: fixed a bug with double quotes when "disable ajax" is set to "yes".

= Version 1.3.3 =
 * Fix: further optimizations with displaying price: now takes into account decimal settings in WooCommerce.

= Version 1.3.2 =
 * Fix: Currency symbols are now being displayed according to your WooCommerce currency settings.
 * Added: filter "woobought-run" so you can easily extend some logic.

= Version 1.3.1 =
 * Update: updated German language errors.

= Version 1.3.0 =
* Update: upgraded CSS to fix mobile issues.
* Fix: fixed SSLVERIFY error.

= Version 1.2.4 =
 * Added: performance enhancing setting to bypass admin-ajax.
 * Update: possibility to go further back in time to fetch notifications.

= Version 1.2.3 =
 * Fix: fixed issue with {{first_name}} in some cases.
 * Update: added product filter so developers can change the output.
 * Update: added Romanian language.

= Version 1.2.2 =
 * Fix: minor bugfixes.

= Version 1.2.1=
 * Added: support for digital products (products without shipping address).

= Version 1.2.0=
 * Added: support for variable products.
 * Update: better support for WooCommerce 3.0 and up.

= Version 1.1.2=
 * Added: Spanish language.
 * Update: verify WP 4.9+ compatibility.

= Version 1.1.1 =
 * Added: German language.

= Version 1.1.0 =
 * Update: rewritten licensing system.
 * Testing: verify WP 4.8.1 compatibility.

= Version 1.0.9 =
 * Added: fallbacks for when buyer info isn't available.
 * Added: new language nl_BE.
 * Added: new language fr_BE.
 * Update: Verify WP 4.7.3 compatibility & latest WooCommerce.

= Version 1.0.8 =
 * Update: minor enhancements & verified WP 4.7 compatibility.

= Version 1.0.7 =
 * Added: set how long you want to store results in cache.

= Version 1.0.6 =
 * Added: added tracking URL option for Google Analytics.

= Version 1.0.5 =
 * Added: possibility to change thumbnail sources.
 * Added: 5 second delay option.

= Version 1.0.4 =
 * Bugfixes

= Version 1.0.3 =
 * Added; option to change the prefetch limit.
 * Added: option to keep looping when there are no new orders to show.
 * Changed: order of appearance: newest first.

= Version 1.0.2 =
 * Bugfixes: fixed issue where WordPress would sometimes save colors without a hash.
 * Update: only show notifications when the thumbnail image is loaded (to prevent Flash of Unrendered Content).

 = Version 1.0.0=
  * Initial release