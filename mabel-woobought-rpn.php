<?php
/*
 * Plugin Name: WooBought Pro
 * Plugin URI: http://woobought.com/
 * Description: Display notifications of recently bought products to your visitors. It feels like everyone is buying! Giving your store social proof, credibility, visibility and it creates urgency.
 * Version: 1.3.4
 * Author: Maarten Belmans
 * Author URI: http://maartenbelmans.com/
 * Text Domain: mabel-woobought-rpn
*/

if(!defined('ABSPATH')){die;}

function run_mabel_rpn(){
	if (!defined('MABEL_WOOBOUGHT_VERSION'))
		define('MABEL_WOOBOUGHT_VERSION', '1.3.4');
	
	if (!defined('MABEL_WOOBOUGHT_NAME'))
		define('MABEL_WOOBOUGHT_NAME', 'WooBought Pro');
	
	if (!defined('MABEL_WOOBOUGHT_DIR'))
		define('MABEL_WOOBOUGHT_DIR', plugin_dir_path( __FILE__ ));

	if(!defined('MABEL_WOOBOUGHT_BASENAME'))
		define('MABEL_WOOBOUGHT_BASENAME',plugin_basename( __FILE__ ));

	if (!defined('MABEL_WOOBOUGHT_SLUG'))
		define('MABEL_WOOBOUGHT_SLUG', trim(dirname(plugin_basename(__FILE__)), '/'));
	
	if (!defined('MABEL_WOOBOUGHT_URL'))
		define('MABEL_WOOBOUGHT_URL', plugin_dir_url(__FILE__));
	
	require MABEL_WOOBOUGHT_DIR . 'includes/class-mabel-woobought.php';

	$plugin = new Mabel_WC_RecentlyPurchased();
}

run_mabel_rpn();