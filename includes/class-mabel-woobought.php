<?php
if(!defined('ABSPATH')){die;}

// Core plugin class
class Mabel_WC_RecentlyPurchased{
	protected $loader;
	protected $defaults;
	protected $settingskey = 'mabel-woobought-settings';
	private $options;
	private $plugin_admin;

	public function __construct(){
		$this->loadDependencies();
		$this->setLanguageSupport();

		$this->loader->add_action('woocommerce_init',$this,'startup');

		// Kickoff
		$this->loader->run();
	}

	public function startup(){
		try{
			$this->checkDependencies();
			$this->setOptions();
			$this->u();
			$this->adminHooks();
			$this->publicHooks();
			$this->loader->run();
			do_action('mabel-wb-wooboughtloaded',$this->options);
		}catch(Exception $e){
			if($e->getCode() === 1){
				add_action( 'admin_notices', function() use($e){
					?>
					<div class="error fade">
						<p>
							<strong><?php echo esc_html($e->getMessage()); ?></strong>
						</p>
					</div>
					<?php
				});
			}
		}
	}

	private function checkDependencies(){
		if(version_compare( WC()->version, '2.3', '<'))
			throw new Exception( MABEL_WOOBOUGHT_NAME.' '.__('requires WooCommerce version 2.3 or higher',MABEL_WOOBOUGHT_SLUG),1);
	}

	private function loadDependencies(){
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mabel-woobought-lang.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mabel-woobought-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-mabel-woobought-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-mabel-woobought-public.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-mabel-woobought-u.php';

		$this->loader = new Mabel_RPN_Loader();
	}

	private function u(){
		new Mabel_RPN_U('https://studiowombat.com/wp-json/ssp/v1');
	}

	private function setOptions(){
		$this->defaults = array(
			'limit' => 15,
			'boxbgcolor'=>'#ffffff',
			'textcolor'=>'#000000',
			'boxplacement'=>'bottom-left',
			'boxlayout' =>'imageleft',
			'boxsize' =>'small',
			'dropshadow'=>'ds-subtle',
			'roundedcorners'=>'rc-none',
			'text' => '{{first_name}} from {{city}}, {{country}} purchased {{product_name}} for {{price}}.',
			'title' => '{{product_name}}',
			'timeago'=>1,
			'hideonmobile'=>0,
			'notificationdelay'=>60,
			'notificationduration'=>10,
			'firstnotification'=>10,
			'hideclose'=>0,
			'excludepages'=>'{}',
			'disablelink'=>1,
			'notificationage'=>7,
			'loop'=>0,
			'trackingurl'=>'',
			'imagesize' =>'shop_thumbnail',
			'cache' => '300',
			'disableajax' => '0'
		);
		$this->options = array_merge($this->defaults, (array)get_option($this->settingskey));
	}
	// Add multi language support.
	private function setLanguageSupport(){
		$lang = new Mabel_RPN_Lang();
		$lang->setDomain( MABEL_WOOBOUGHT_SLUG);
		$this->loader->add_action( 'plugins_loaded', $lang, 'loadTextDomain' );
	}

	// Register all of the hooks related to the admin area functionality of the plugin.
	private function adminHooks(){
		$this->plugin_admin = new Mabel_RPN_Admin( $this->defaults,$this->options,$this->settingskey);
		// ajax functions
		$this->loader->add_action('wp_ajax_nopriv_mabel-rpn-getnew-purchased-products',$this->plugin_admin,'getNewestPurchasedProducts'); // if user is not logged in
		$this->loader->add_action('wp_ajax_mabel-rpn-getnew-purchased-products',$this->plugin_admin,'getNewestPurchasedProducts'); // if user is logged in
		// Add settings
		$this->loader->add_action( 'admin_init', $this->plugin_admin, 'initSettings');
		// Add menu item
		$this->loader->add_action( 'admin_menu', $this->plugin_admin, 'addSettingsMenu' );
		// Notices
		$this->loader->add_action('admin_notices', $this->plugin_admin, 'show_admin_notices');

		// load scripts & styles
		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueueScripts' );
		$this->loader->add_action( 'admin_enqueue_scripts', $this->plugin_admin, 'enqueueStyles' );

		// Add Settings link to the plugin
		$plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . MABEL_WOOBOUGHT_SLUG . '.php' );
		$this->loader->add_filter( 'plugin_action_links_' . $plugin_basename, $this->plugin_admin, 'addSettingsLinkToPlugin' );
	}

	// Register all of the hooks related to the public-facing functionality of the plugin.
	private function publicHooks() {
		$plugin_public = new Mabel_RPN_public($this->defaults,$this->options,$this->settingskey );
		// load scripts & styles
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueueScripts' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueueStyles' );

		if($this->options['disableajax'] == 1) {
			$products = $this->plugin_admin->getNewestPurchasedProducts(true);
			add_action('wp_footer', function() use ($products){
				echo "<script>var woobought_data = ".json_encode($products).";</script>";
			});
		}
	}
}