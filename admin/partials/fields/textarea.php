<?php
	if(!defined('ABSPATH')){die;}
?>
<div class="multi-choice-wrapper">
	<input type="hidden" name="<?php echo $this->settingskey.'['.$id.']';?>" value='<?php echo json_encode($values,JSON_FORCE_OBJECT); ?>'/>
	<div class="chosen">
		<?php
			echo '<em class="infotext" style="'.(empty($values)?'':'display:none;').'">'.__('Choose from the pages below',MABEL_WOOBOUGHT_SLUG).'</em>';
			
			foreach($values as $key=>$value){
				echo '<span class="mc-choice" data-id="'.$key.'">'.$value.' <span class="mc-close">&times;</span></span>';
			}
		?>
	</div>
	<div class="options">
		<div class="mc-o-specials">
			<span class="mc-option" data-id="-3"><?php _e('Front page',MABEL_WOOBOUGHT_SLUG); ?></span>
			<span class="mc-option" data-id="-2"><?php _e('Posts page',MABEL_WOOBOUGHT_SLUG); ?></span>
			<span class="mc-option" data-id="-1"><?php _e('All blog posts',MABEL_WOOBOUGHT_SLUG); ?></span>
		</div>
		<?php
			foreach($pages as $page){
				echo '<span class="mc-option" data-id="'.$page->ID.'">'.$page->post_title.'</span>';
			}
		?>
	</div>
</div>
<?php 
if(isset($comment))
	echo '<div><em class="infotext">'.__($comment,MABEL_WOOBOUGHT_SLUG).'</em></div>';
?>