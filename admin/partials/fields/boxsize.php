<?php
	if(!defined('ABSPATH')){die;}
?>
<table class="form-table">
	<tr>
		<td>
			<div class="radio-img-option">
				<input onchange="WooBought.Application.changeExample(this,'boxsize')" type="radio" id="r-large" name="<?php echo $this->settingskey; ?>[boxsize]" value="large" <?php checked( 'large' == $selected ); ?>>
				<label for="r-large">
					<img src="<?php echo  $this->imagebaseurl; ?>/large.png"/>
					<span><?php _e('Large',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
		<td>
			<div class="radio-img-option">
				<input onchange="WooBought.Application.changeExample(this,'boxsize')" type="radio" id="r-med" name="<?php echo $this->settingskey; ?>[boxsize]" value="medium" <?php checked( 'medium' == $selected ); ?>>
				<label for="r-med">
					<img src="<?php echo  $this->imagebaseurl; ?>/medium.png"/>
					<span><?php _e('Medium',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
		<td>
			<div class="radio-img-option">
				<input onchange="WooBought.Application.changeExample(this,'boxsize')" type="radio" id="r-small" name="<?php echo $this->settingskey; ?>[boxsize]" value="small" <?php checked( 'small' == $selected ); ?>>
				<label for="r-small">
					<img src="<?php echo $this->imagebaseurl; ?>/small.png"/>
					<span><?php _e('Small',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
	</tr>
</table>