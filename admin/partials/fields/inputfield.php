<?php
	if(!defined('ABSPATH')){die;}
?>
<input type="text" value="<?php echo $value; ?>" name="<?php echo $this->settingskey.'['.$id.']'; ?>"/>
<?php
	if($this->options['licensekey']){
		if($this->options['ldate'])
			echo '<span style="color:#2cbb2c;font-weight:bold;"> '.__('License valid until',MABEL_WOOBOUGHT_SLUG).' '.date('Y-m-d',strtotime($this->options['ldate'])).'</span>';
		else
			echo '<span style="font-weight:bold;color:#ff0000;"> '.__('Your license is invalid. Please renew if you want to receive updated, bugfixes and support',MABEL_WOOBOUGHT_SLUG).'</span>';
	}
?>