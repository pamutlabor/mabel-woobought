<?php
	if(!defined('ABSPATH')){die;}
?>
<table class="form-table">
	<tr>
		<td>
			<div class="radio-img-option">
				<input type="radio" id="r-bl" name="<?php echo $this->settingskey; ?>[boxplacement]" value="bottom-left" <?php checked( 'bottom-left' == $selected ); ?>>
				<label for="r-bl">
					<img src="<?php echo  $this->imagebaseurl; ?>/bottom_left.png"/>
					<span><?php _e('Bottom left',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
		<td>
			<div class="radio-img-option">
				<input type="radio" id="r-br" name="<?php echo $this->settingskey; ?>[boxplacement]" value="bottom-right" <?php checked( 'bottom-right' == $selected ); ?>>
				<label for="r-br">
					<img src="<?php echo  $this->imagebaseurl; ?>/bottom_right.png"/>
					<span><?php _e('Bottom right',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="radio-img-option">
				<input type="radio" id="r-tl" name="<?php echo $this->settingskey; ?>[boxplacement]" value="top-left" <?php checked( 'top-left' == $selected ); ?>>
				<label for="r-tl">
					<img src="<?php echo  $this->imagebaseurl; ?>/top_left.png"/>
					<span><?php _e('Top left',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
		<td>
			<div class="radio-img-option">
				<input type="radio" id="r-tr" name="<?php echo $this->settingskey; ?>[boxplacement]" value="top-right" <?php checked( 'top-right' == $selected ); ?>>
				<label for="r-tr">
					<img src="<?php echo  $this->imagebaseurl; ?>/top_right.png"/>
					<span><?php _e('Top right',MABEL_WOOBOUGHT_SLUG); ?></span>
				</label>
			</div>
		</td>
	</tr>
</table>