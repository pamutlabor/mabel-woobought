<?php
	if(!defined('ABSPATH')){die;}

	$info = Mabel_RPN_U::get_license_info();
	$time_left_in_days = '';
	if($info !== null)
	{
		$licensekey = !empty($info->key);
		$license_date_as_date =  DateTime::createFromFormat(
			'Y-m-d H:i:s',
			$info->expiration,
			new DateTimeZone('UTC'));
		$difference =  $license_date_as_date->diff(new DateTime('now',new DateTimeZone('UTC')));
		$time_left_in_days = $difference->days;
		$license_overdue = $difference->invert === 0;
	}else{
		$licensekey = false;
	}
?>

<div class="wrap">
	<div id="translations" data-outofstock="<?php _e('Out of stock','woocommerce'); ?>"></div>
	<h1><?php echo esc_html(get_admin_page_title());?></h1>
	<h2 class="nav-tab-wrapper">
		<?php if($licensekey){ ?>
			<a data-tab="box_options" href="#" class="nav-tab nav-tab-active"><i class="dashicons dashicons-admin-customizer"></i> <span><?php _e('Notification design',MABEL_WOOBOUGHT_SLUG);?></span></a>
			<a data-tab="text_options" href="#" class="nav-tab"><i class="dashicons dashicons-editor-aligncenter"></i> <span><?php _e('Message',MABEL_WOOBOUGHT_SLUG);?></span></a>
			<a data-tab="display_options" href="#" class="nav-tab"><i class="dashicons dashicons-admin-settings"></i> <span><?php _e('Display Options',MABEL_WOOBOUGHT_SLUG);?></span></a>
			<a data-tab="advanced_options" href="#" class="nav-tab"><i class="dashicons dashicons-admin-generic"></i> <span><?php _e('Advanced',MABEL_WOOBOUGHT_SLUG);?></span></a>
			<?php do_action('mabel-wb-tabheader'); ?>
		<?php } ?>
		<a data-tab="license_options" href="#" class="nav-tab <?php if(!$licensekey) echo ' nav-tab-active '; ?>"><i class="dashicons dashicons-admin-network"></i> <span><?php _e('License',MABEL_WOOBOUGHT_SLUG);?></span></a>
	</h2>
	<?php if($licensekey){ ?>
	<form action="options.php" id="mabel-rpn-form" method="POST">
		<div class="tab tab-box_options">
			<?php
				settings_fields('box-options');
				do_settings_sections( 'box-options');
				submit_button(__('Save All Changes',MABEL_WOOBOUGHT_SLUG));
			?>
		</div>
		<div class="tab tab-text_options" style="display:none;">
			<?php
				settings_fields('text-options');
				do_settings_sections( 'text-options');
				submit_button(__('Save All Changes',MABEL_WOOBOUGHT_SLUG));
			?>
		</div>
		<div class="tab tab-display_options" style="display:none;">
			<?php
				settings_fields('display-options');
				do_settings_sections( 'display-options');
				submit_button(__('Save All Changes',MABEL_WOOBOUGHT_SLUG));
			?>
		</div>
		<div class="tab tab-advanced_options" style="display:none;">
			<?php
			settings_fields('advanced-options');
			do_settings_sections( 'advanced-options');
			submit_button(__('Save All Changes',MABEL_WOOBOUGHT_SLUG));
			?>
		</div>
		<?php do_action('mabel-wb-tabs'); ?>
	</form>
	<?php } ?>
	<div class="tab tab-license_options" style="<?php if($licensekey) echo'display:none;';?>">
		<form id="mabel-rpn-license-form" method="POST" action="admin-post.php">
		<input type="hidden" name="action" value="<?php echo MABEL_WOOBOUGHT_SLUG .($licensekey ? '-deactivate-license' : '-activate-license'); ?>">
		<?php
			if(!$licensekey) echo '<p>Enter your license key to use the plugin.</p>';
		?>
			<input style="margin-bottom:15px;padding:8px;"
				class="widefat"
				placeholder="Your license key"
				value="<?php echo $licensekey ? '*********************' : ''; ?>"
				type="<?php echo $licensekey ? 'password' : 'text'; ?>"
				     <?php echo $licensekey ? 'readonly="readonly"' : 'name="'. MABEL_WOOBOUGHT_SLUG .'-license"'; ?>
			/>

			<?php if($licensekey){ ?>
				<?php if($license_overdue) { ?>
					<p style="padding-top:15px;">
						<?php echo sprintf(__('Your license <b class="msg msg-bad">is invalid</b> and %s days overdue.', MABEL_WOOBOUGHT_SLUG),$time_left_in_days); ?>
						<br/>
						<?php _e("When your license runs out, the software will still function, but you won't be able to install updates, receive bugfixes or get support.",MABEL_WOOBOUGHT_SLUG); ?>
					</p>
				<?php } else { ?>
					<p style="padding-top:15px;">
						<?php echo sprintf(__('Your license <b class="msg msg-good">is valid</b> for %s more days.', MABEL_WOOBOUGHT_SLUG), $time_left_in_days); ?>
						<br/>
						<?php _e("When your license runs out, the software will still function, but you won't be able to install updates, receive bugfixes or get support.", MABEL_WOOBOUGHT_SLUG); ?>
					</p>
				<?php } ?>
			<?php } ?>

			<div class="p-t-3">
				<input type="submit" name="submit" id="submit" class="button button-primary" value="<?php echo __( $licensekey ? 'Deactivate license' : 'Activate license', MABEL_WOOBOUGHT_SLUG); ?>" />
			</div>

		</form>
	</div>
	<?php if($licensekey){ ?>
	<table class="form-table">
		<tr><th style="padding-top: 40px;"><?php _e('Example',MABEL_WOOBOUGHT_SLUG); ?></th><td>
		<div class="example-wrapper">
			<div class="rpn-example">
				<div class="rpn-example-img imageleft" style="background-image:url(<?php echo $this->imagebaseurl;?>/example.jpg)"></div>
				<div class="message-wrapper mabel-rpn-message">
					<h4>Dell Inspiron Desktop</h4>
					<span class="message"></span>
					<div class="timeago"><?php _e('7 days ago',MABEL_WOOBOUGHT_SLUG); ?></div>
					<span class="mabel-rpn-hide">
						<a class="rpn-disable-btn"><?php _e("Disable",MABEL_WOOBOUGHT_SLUG); ?></a>
					</span>
				</div>
				<div class="rpn-example-img imageright" style="display:none;background-image:url(<?php echo $this->imagebaseurl;?>/example.jpg)"></div>
				<a class="mabel-rpn-close">×</a>
			</div>
		</div>
		</td></tr>
	</table>
	<?php } ?>
</div>