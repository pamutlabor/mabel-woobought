<?php
if(!defined('ABSPATH')){die;}

class Mabel_RPN_Admin{
	private $options;
	private $defaults;
	private $settingskey;
	private $imagebaseurl;
	private static $notices = array();

	public function __construct($defaults,$options,$settingskey){
		$this->defaults = $defaults;
		$this->imagebaseurl = MABEL_WOOBOUGHT_URL.'/admin/img';
		$this->options = $options;
		$this->settingskey = $settingskey;
	}

	public function getNewestPurchasedProducts($returnthem = true){
		try {
			$cachekey = 'mabel-woobought-cached-products';
			$products =  get_transient( $cachekey );
			if ( ! $products ) {
				global $wpdb;

				$args = array(
					'post_type' => 'shop_order',
					'post_status' => 'wc-on-hold, wc-completed, wc-pending, wc-processing',
					'orderby' => 'ID',
					'order' => 'DESC',
					'posts_per_page' => $this->getOption( 'limit' ),
					'date_query' => array(
						'after' => date('Y-m-d', strtotime('-'.$this->getOption( 'notificationage' ).' days'))
					)
				);

				$posts = get_posts($args);
				$products = array();
				$version_is_before_3 = version_compare( WC()->version, '3.0', '<')?true:false;

				foreach($posts as $post) {

					$order = new WC_Order($post->ID);
					$order_items = $order->get_items();

					if(!empty($order_items)) {
						$first_item = array_values($order_items)[0];
						$product_id = $first_item['variation_id'] == '0' ? $first_item['product_id'] : $first_item['variation_id'];
						$product = wc_get_product($product_id);
						if(!empty($product)){
							preg_match( '/src="(.*?)"/', $product->get_image($this->getOption('imagesize')), $imgurl);

							$p = array(
								'id'    => $first_item['order_id'],
								'name'  => $product->get_title(),
								'url'   => $product->get_permalink(),
								'date'  => $post->post_date_gmt,
								'image' => count($imgurl) === 2 ? $imgurl[1] : null,
								'stock' => $product->get_availability(),
								'price' => $this->formatProductPrice($version_is_before_3 ? $product->get_display_price() : wc_get_price_to_display($product) ),
								'buyer' => $this->createBuyerArray($order)
							);

							$p = apply_filters('woobought_product_data',$p);

							array_push( $products, $p );
						}
					}
				}

				set_transient( $cachekey, $products, intval($this->getOption('cache')) ); // Cache the results for x minutes
			}

			if(!$returnthem){
				echo json_encode($products);
				wp_die();
			}
			else return json_encode($products);

		}catch(Exception $e){
			if(!$returnthem)
				wp_die();
			else return '';
		}
	}

	private function formatProductPrice($price) {
	    if(empty($price))
	        $price = 0;

		return sprintf(
		    get_woocommerce_price_format(),
            get_woocommerce_currency_symbol(),
            number_format($price,wc_get_price_decimals(),wc_get_price_decimal_separator(),wc_get_price_thousand_separator())
        );
	}

    private function createBuyerArray($order){
	    $billing_address = $order->get_address('billing');
	    $address = $order->get_address('billing');

	    if(!isset($address['city']) || strlen($address['city']) == 0 )
			$address = $order->get_address('shipping');

	    $buyer = array(
		    'name' => isset($billing_address['first_name']) && strlen($billing_address['first_name']) > 0 ? ucfirst($billing_address['first_name']) : $this->t('someone'),
		    'city' => isset($address['city']) && strlen($address['city']) > 0 ? ucfirst($address['city']) : 'N/A',
		    'state' => isset($address['state']) && strlen($address['state']) > 0 ? ucfirst($address['state']) : 'N/A',
		    'country' =>  isset($address['country']) && strlen($address['country']) > 0 ? WC()->countries->countries[$address['country']] : 'N/A',
	    );

        return $buyer;
    }

	public function enqueueStyles() {
		if(isset($_GET['page']) && $_GET['page'] == MABEL_WOOBOUGHT_SLUG){
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( MABEL_WOOBOUGHT_SLUG, MABEL_WOOBOUGHT_URL.'/admin/css/mabel-woobought-admin.min.css', array(), MABEL_WOOBOUGHT_VERSION, 'all' );
		}
	}
	
	// Register js for the admin area.
	public function enqueueScripts() {
		if(isset($_GET['page']) && $_GET['page'] == MABEL_WOOBOUGHT_SLUG){
			wp_enqueue_script(MABEL_WOOBOUGHT_SLUG, MABEL_WOOBOUGHT_URL . '/admin/js/mabel-woobought-admin.min.js', array('jquery','wp-color-picker'), MABEL_WOOBOUGHT_VERSION, false);
		}
	}
	
	// Add a menu item in Dashboard>settings if you have the 'manage options' right and draw its options page via settings.php.
	public function addSettingsMenu(){
		add_options_page('Plugin '.$this->t('Settings'), MABEL_WOOBOUGHT_NAME, 'manage_options', MABEL_WOOBOUGHT_SLUG, array($this,'displaySettingsPage'));
	}
	public function displaySettingsPage(){
        include_once('partials/settings.php');
    }
	public function addSettingsLinkToPlugin( $links ) {
		$settings_link = array('<a href="' . admin_url( 'options-general.php?page=' . MABEL_WOOBOUGHT_SLUG ) . '">' . $this->t('Settings'). '</a>');
		return array_merge(  $settings_link, $links );
	}
	
	// Register sections & settings
	public function initSettings(){
		register_setting( 'box-options', $this->settingskey);
		add_settings_section("section", "", null,'box-options');
		
		add_settings_field('boxlayout',$this->t('Layout'),array($this,'displayBoxLayout'),'box-options','section');
		add_settings_field('boxplacement',$this->t('Placement'),array($this,'displayBoxPlacement'),'box-options','section');
		add_settings_field('boxsize',$this->t('Size'),array($this,'displayBoxSize'),'box-options','section');
		add_settings_field('boxbgcolor',$this->t('Background color'),array($this,'displayColorpicker'),'box-options','section',array('id'=>'boxbgcolor'));
		add_settings_field('dropshadow',$this->t('Drop shadow'),array($this,'displayDropDown'),'box-options','section',array('id'=>'dropshadow','options'=>array($this->t('No drop shadow')=>'ds-none',$this->t('Subtle')=>'ds-subtle',$this->t('Medium')=>'ds-medium',$this->t('Heavy')=>'ds-heavy')));
		add_settings_field('roundedcorners',$this->t('Rounded corners'),array($this,'displayDropDown'),'box-options','section',array('id'=>'roundedcorners','options'=>array($this->t('No rounded corners')=>'rc-none',$this->t('Subtle')=>'rc-subtle',$this->t('Medium')=>'rc-medium',$this->t('Heavy')=>'rc-heavy'),'comment'=>$this->t("Round the corners of the notification box")));
		
		register_setting( 'text-options', $this->settingskey,array($this,'sanitizeInput'));
		add_settings_section("section2", "", null,'text-options');
		
		add_settings_field('textcolor',$this->t('Color'),array($this,'displayColorpicker'),'text-options','section2',array('id'=>'textcolor'));
		add_settings_field('title',$this->t('Title'),array($this,'displayDropDown'),'text-options','section2',array('id'=>'title','options'=>array('Product name'=>'{{product_name}}','Product name + price'=>'{{product_name}} - {{price}}','Product name + stock indication'=>'{{product_name}} ({{stock}})','Product name + price + stock indication'=>'{{product_name}} - {{price}} ({{stock}})')));
		add_settings_field('text',$this->t('Message'),array($this,'displayTextOption'),'text-options','section2');
		add_settings_field('timeago',$this->t("Include 'time ago'") ,array($this,'displayDropDown'),'text-options','section2',array('id'=>'timeago','options'=>array($this->t('Yes')=>1,$this->t('No')=>0),'comment'=>$this->t("Include how long ango a product was purchased. Example: 15 minutes ago")));
		add_settings_field('disablelink',$this->t("Include disable link") ,array($this,'displayDropDown'),'text-options','section2',array('id'=>'disablelink','comment'=>$this->t("Include a 'Disable' button so visitors can disable notifications"),'options'=>array($this->t("Don't show")=>0,$this->t('Always show')=>1,$this->t("Show every 3 notifications")=>3)));

		register_setting( 'display-options', $this->settingskey);
		add_settings_section("section3", "", null,'display-options');
		
		add_settings_field('hideonmobile',$this->t("Hide notifications on mobile") ,array($this,'displayDropDown'),'display-options','section3',array('id'=>'hideonmobile','options'=>array($this->t('Yes')=>1,$this->t('No')=>0)));
		add_settings_field('hideclose',$this->t("Hide close button") ,array($this,'displayDropDown'),'display-options','section3',array('id'=>'hideclose','options'=>array($this->t('Yes')=>1,$this->t('No')=>0)));

		$s = $this->t('seconds');
		$ms = $this->t('minutes');
		$ds = $this->t('days');
		
		add_settings_field('notificationage',$this->t("Don't show purchases older than"),array($this,'displayDropDown'),'display-options','section3',array('id'=>'notificationage','options'=>array('1 '.$this->t('day')=>1,'2 '.$ds=>2,'3 '.$ds=>3,'5 '.$ds=>5,'1 '.$this->t('week')=>7,'10 '.$ds=>10,'2 '.$this->t('weeks')=>14,'3 '.$this->t('weeks')=>21,'4 '.$this->t('weeks')=>28,'8 '.$this->t('weeks')=>56,'12 '.$this->t('weeks')=>84, '24 '.$this->t('weeks')=>168,'52 '.$this->t('weeks')=>365)));
		add_settings_field('notificationdelay',$this->t("Time between notifications") ,array($this,'displayDropDown'),'display-options','section3',array('id'=>'notificationdelay','options'=>array('5 '.$s=>5,'10 '.$s=>10,'20 '.$s=>20,'30 '.$s=>30,'40 '.$s=>40,'50 '.$s=>50,'1 '.$this->t('minute')=>60,'1.5 '.$ms=>90,'2 '.$ms=>120),'comment'=>$this->t("The time to wait before showing the next notification")));
		add_settings_field('notificationduration',$this->t("Notification duration") ,array($this,'displayDropDown'),'display-options','section3',array('id'=>'notificationduration','options'=>array('5 '.$s=>5,'8 '.$s=>8,'10 '.$s=>10,'15 '.$s=>15,'20 '.$s=>20),'comment'=>$this->t("How long the notification should be displayed before auto closing")));
		add_settings_field('firstnotification',$this->t("Time to 1st notification") ,array($this,'displayDropDown'),'display-options','section3',array('id'=>'firstnotification','options'=>array('3 '.$s=>3,'5 '.$s=>5,'8 '.$s=>8,'10 '.$s=>10,'15 '.$s=>15,'20 '.$s=>20,'30 '.$s=>30),'comment'=>$this->t("Time between page load and first notification")));
		add_settings_field('excludepages',$this->t("Exclude on these pages") ,array($this,'displayExcludeList'),'display-options','section3',array('id'=>'excludepages','comment'=>$this->t("If you don't want the notifications to appear on certain pages, denote them here")));

        register_setting('advanced-options',$this->settingskey);
        add_settings_section("section5", "", null,'advanced-options');
        add_settings_field('trackingurl',$this->t('Tracking URL'),array($this,'displayTextField'),'advanced-options','section5',array('id'=>'trackingurl','placeholder'=>'utm_source=mywebsite&utm_medium=web&utm_campaign=my-campaign'));
        add_settings_field('cache',$this->t('Cache results'),array($this,'displayDropDown'),'advanced-options','section5',array('id'=>'cache','comment'=>'How long to cache the results fetched from the database.','options'=>array('1 '.$ms=>'60','5 '.$ms =>'300','10 '.$ms=>'600','60 '.$ms=>'3600','6 '.$this->t('hours')=>'21600','12 '.$this->t('hours')=>'43200')));
        add_settings_field('limit',$this->t('Prefetch limit'),array($this,'displayDropDown'),'advanced-options','section5',array('id'=>'limit','options'=>array('15'=>'15','20'=>'20','25'=>'25','35'=>'35'),'comment'=>$this->t('Fetch this many orders at once. The higher the limit, the more notifications will be shown.')));
        add_settings_field('loop',$this->t('Loop notifications'),array($this,'displayDropDown'),'advanced-options','section5',array('id'=>'loop','options'=>array('No'=>'0','Yes'=>'1'),'comment'=>$this->t('When all notifications are shown and there are no new orders yet, show the old notifications again.')));
        add_settings_field('imagesize',$this->t('Image size'),array($this,'displayDropDown'),'advanced-options','section5',array('id'=>'imagesize','options'=>array('shop_thumbnail (smallest)'=>'shop_thumbnail','shop_catalog (medium)'=>'shop_catalog','shop_single (largest)'=>'shop_single'),'comment'=>$this->t('If the notification images are pixelated, select a larger size here.')));
        add_settings_field('disableajax',$this->t('Disable ajax'),array($this,'displayDropDown'),'advanced-options','section5',array('id'=>'disableajax','options'=>array('No'=>'0','Yes'=>'1'),'comment'=>$this->t('Is admin-ajax too slow for you? You can speed up initial load by disabling the first ajax call with this setting. If disabled, the first notifications will be loaded straight in your HTML. ONLY change this setting when your site has performance issues!')));

	}
	
	// Display functions

    public function displayTextField($args){
        $id = $args['id'];
        $value = $this->getOption($id);
        $placeholder = isset($args['placeholder'])?$args['placeholder']:'';
        require('partials/fields/textfield.php');
    }

	public function displayDropDown($args){
		$options = $args['options'];
		$id = $args['id'];
		$comment = isset($args['comment'])?$args['comment']:null ;
		require('partials/fields/dropdownlist.php');
	}
	public function displayExcludeList($args){
		$id = $args['id'];
		$comment = $args['comment'];
		$values = json_decode($this->getOption($id),true);
		$pages = get_pages(array('post_type' => 'page'));
		require('partials/fields/textarea.php');
	}
	public function displayTextOption(){
		$value = $this->getOption('text');
		require('partials/fields/textoption.php');
	}
	public function displayBoxSize(){
		$selected = $this->getOption('boxsize');
		require('partials/fields/boxsize.php');
	}
	public function displayBoxLayout(){
		$selected = $this->getOption('boxlayout');
		require('partials/fields/boxlayout.php');
	}
	public function displayBoxPlacement(){
		$selected = $this->getOption('boxplacement');
		require('partials/fields/boxplacement.php');
	}
	public function displayColorpicker($args){
		$id = $args['id'];
		$value = $this->getOption($id);
		require('partials/fields/colorpicker.php');
	}
	
	// Sanitizing
	public function sanitizeInput($input){
		$output = array();
		/*$whitelist = array(
			'a' => array('href' => array(),'style'=>array(),'class'=>array()),
			'br' => array('style'=>array(),'class'=>array()),
			'span' => array('style'=>array(),'class'=>array()),
			'i' => array('class'=>array())
		);*/
		// Loop through each of the incoming options
		foreach( $input as $key => $value ) {
			// Check to see if the current option has a value. If so, process it.
			if(isset($input[$key])){
				//if($key === 'text')
					//$output[$key] = esc_html(wp_kses($input[$key],$whitelist));
				//else 
				$output[$key] = $input[$key];//sanitize_text_field($input[$key]);
			}
		}
		// Return the array processing any additional functions filtered by this action
		return $output;
	}

	public function show_admin_notices() {
		$notices = self::$notices;

		foreach( $notices as $notice ) {
			echo '<div class="notice is-dismissible notice-'.$notice['class'].'"><p>'.$notice['message'].'</p></div>';
		}

		if(isset($_GET['notice']) && isset($_GET['page']) && $_GET['page'] === MABEL_WOOBOUGHT_SLUG){
			$notice = unserialize(base64_decode($_GET['notice']));
			echo '<div class="notice is-dismissible notice-'.$notice['class'].'"><p>'.$notice['message'].'</p></div>';
		}
	}

	// Private Helpers
    private function getOption($id){
        $o = $this->options[$id];
        return (isset($o) && $o!=='')?$o:$this->defaults[$id];
    }

	private function t($key,$echo = false){
		if($echo) _e($key,MABEL_WOOBOUGHT_SLUG);
		else return __($key,MABEL_WOOBOUGHT_SLUG);
	}
	
	private function valueortranslatedefault($key){
		return (empty($this->options[$key])? $this->t($this->defaults[$key]) : $this->options[$key]);
	}
}