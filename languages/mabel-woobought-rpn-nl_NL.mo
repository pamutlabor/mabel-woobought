��    a      $  �   ,      8     9     A  
   J  	   U     _  
   h     s     |     �  %   �     �     �     �     �     �     	     	     	  
   /	     :	     Z	  (   f	     �	     �	  [   �	  
   

     
     
     -
  A   J
  8   �
  D   �
  P   
  
   [     f  
   r     }  @   �     �  F   �     -     3     :     B     N     b     u     |     �     �     �     �     �     �  	   �  
   �     �  )         *     :     K     T     o     t     z     �  5   �     �  -   �               $  	   -  c   7     �  Z   �     �           	                    $     (     -     2     8     K     R  	   Z  *   d     �     �     �     �  R   �  �   �     �     �  
   �  
   �                    #     7  (   D     m  
   ~     �     �     �     �     �     �     �     �       6     	   Q     [  x   y  
   �     �            B   2  8   u  Y   �  ]        f     w     �     �  R   �       M        f     l     t     }     �     �     �     �     �     �     �     �     �     �          	           '     H     U     c     p     �     �     �     �  2   �     �  4   �     $     ;  
   A     L  m   X     �  c   �     -  
   5  	   @     J     S     [     c     g     m     q     u     �     �     �  '   �     �     �     �     �  N   �     `      G      @       ^   O   Z           Y       U       C   a   L   =   [   I   7      W   T   F   "       8                   -   %   (       )   #      :   &      V      >   $   2   <   \   N              1   3           5          '      .   A   9   E       K          J      M          _   /   ,   Q       +                 R      D       ?   ;   H                                !      B               P         4      0   	   ]   
   X       6       *                      S           %d days %d hours %d minutes %d months %d years 7 days ago Advanced All blog posts Always show Available codes between double braces Background color Bottom left Bottom right Cache results Choose from the pages below Color Disable Display Options Don't show Don't show purchases older than Drop shadow Enter your license key to use the plugin Example Exclude on these pages Fetch this many orders at once. The higher the limit, the more notifications will be shown. Front page Heavy Hide close button Hide notifications on mobile How long the notification should be displayed before auto closing How long to cache the results fetched from the database. If the notification images are pixelated, select a larger size here. If you don't want the notifications to appear on certain pages, denote them here Image left Image right Image size Include 'time ago' Include a 'Disable' button so visitors can disable notifications Include disable link Include how long ango a product was purchased. Example: 15 minutes ago Large Layout License License key License valid until Loop notifications Medium Message No No drop shadow No image No rounded corners Notification design Notification duration Placement Posts page Prefetch limit Round the corners of the notification box Rounded corners Save All Changes Settings Show every 3 notifications Size Small Subtle Text The time to wait before showing the next notification Time between notifications Time between page load and first notification Time to 1st notification Title Top left Top right When all notifications are shown and there are no new orders yet, show the old notifications again. Yes Your license is invalid. Please renew if you want to receive updated, bugfixes and support a day a minute a month a year ago an hour day days hour hours less than a minute minute minutes prefixago requires WooCommerce version 2.3 or higher seconds someone week weeks {{first_name}} from {{city}},{{country}} purchased {{product_name}} for {{price}}. Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl
X-Generator: Poedit 1.8.8
 %d dagen %d uren %d minuten %d maanden %d jaren 7 dagen geleden Extra Alle blog berichten Altijd tonen Mogelijke codes tussen dubbele accolades Achtergrondkleur Linksonder Rechtsonder Cache resultaten Kies uit onderstaande pagina's Kleur Uitschakelen Beeld Opties Nooit tonen Toon geen aankopen ouder dan Slagschaduw Voer uw licentiesleutel in om deze plugin te gebruiken Voorbeeld Uitschakelen op deze pagina's Haal zoveel orders in 1 keer op. Een hoger getal betekent dat meer meldingen getoond worden alvorens nieuwe op te halen. Voorpagina Veel Verberg sluit-knop Verberg de meldingen op GSMs Hoe lang moet de melding te zien zijn alvorens zichzelf te sluiten Hoe lang de resultaten uit de database in cache opslaan. Als de afbeeldingen in de meldingen korrelig zijn, selecteer hier dan een groter formaat. Als je niet wil dat meldingen op bepaalde pagina's getoont worden, kan je dat hier aanduiden. Afbeelding links Afbeelding rechts Afbeeldingsgrootte Toon 'tijd geleden' Toon een 'Uitschakelen' knop zodat bezoekers zelf de meldingen kunnen uitschakelen Toon uitschakel-link Toon hoe lang geleden een product werd gekocht. Voorbeeld: 15 minuten geleden Groot Lay-out Licentie Licentiesleutel Licentie geldig tot Notificatielus Medium Bericht Nee Geen Geen afbeelding Geen Melding ontwerp Duratie van melding Positie Berichtpagina Prefetch limiet Rond de hoeken van de melding af Ronde hoeken Alles Opslaan Instellingen Toon bij elke 3 meldingen Grootte Klein Subtiel Tekst De wachttijd alvorens de volgende melding te tonen Tijd tussen meldingen Tijd tussen het laden van de pagina en de 1e melding Tijd tot de 1e melding Titel Linksboven Rechtsboven Als alle meldingen getoond zijn en er zijn nog geen nieuwe bestellingen, toon dan de oude meldingen nog eens. Ja Uw licentie is ongeldig. Gelieve te vernieuwen als u updates en ondersteuning wil blijven ontvangen een dag een minuut een maand een jaar geleden een uur dag dagen uur uur minder dan een minuut minuut minuten   vereist WooCommerce versie 2.3 of hoger seconden iemand week weken {{first_name}} uit {{city}},{{country}} kocht {{product_name}} voor {{price}}. 