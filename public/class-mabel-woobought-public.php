<?php
if(!defined('ABSPATH')){die;}

class Mabel_RPN_Public {
	private $options;
	private $defaults;
	private $settingskey;
	
	public function __construct($defaults,$options,$settingskey ) {
		$this->defaults = $defaults;
		$this->options = $options;
		$this->settingskey = $settingskey;
	}

	// Register frontend CSS
	public function enqueueStyles(){
		wp_enqueue_style( MABEL_WOOBOUGHT_SLUG, MABEL_WOOBOUGHT_URL . '/public/css/mabel-woobought-public.min.css', array(), MABEL_WOOBOUGHT_VERSION, 'all' );
	}
	
	// Register frontend js.
	public function enqueueScripts(){
		wp_enqueue_script(MABEL_WOOBOUGHT_SLUG, MABEL_WOOBOUGHT_URL . '/public/js/mabel-woobought-public.min.js',array('jquery'), MABEL_WOOBOUGHT_VERSION, true);
		
		// Add settings to frontend javascript
		$translation_array = array(
			'disable' => $this->t( 'Disable'),
			'message' => $this->valueOrTranslateDefault('text'),
			'title' => $this->getOption('title')
		);
		$timeagotranslations = array(
			'suffixAgo' => trim($this->t('ago')), // trim because empty space in translation should be null. We can't translate empty string
			'prefixAgo' => trim($this->t('prefixago')),
			'seconds' => $this->t('less than a minute'),
			'minute' => $this->t('a minute'),
			'minutes' => $this->t('%d minutes'),
			'hour' => $this->t('an hour'),
			'hours' => $this->t('%d hours'),
			'day' => $this->t('a day'),
			'days' => $this->t('%d days'),
			'month' => $this->t('a month'),
			'months' => $this->t('%d months'),
			'year' => $this->t('a year'),
			'years' => $this->t('%d years'),
			'wordSeparator' => ' '
		);
        $alloptions = $this->mergeOptions();
        if(get_option(MABEL_WOOBOUGHT_SLUG.'_license'))
            $alloptions['haslicense'] = true;

		wp_localize_script(MABEL_WOOBOUGHT_SLUG,'mabelrpnsettings', json_encode($alloptions));
		wp_localize_script(MABEL_WOOBOUGHT_SLUG,'ajaxurl', admin_url('admin-ajax.php'));
		wp_localize_script(MABEL_WOOBOUGHT_SLUG, 'rpntranslations', $translation_array );
		wp_localize_script(MABEL_WOOBOUGHT_SLUG, 'rpntimeagotranslations', $timeagotranslations );
		
		// Do we have to run it?
		$exclude = json_decode($this->getOption('excludepages'),true);
		
		$run = true;

		if(count($exclude)>0) {
			if((is_front_page() && isset($exclude[-3]))||(is_home() && isset($exclude[-2]))|| (is_single() && isset($exclude[-1])))
				$run = false;
			else{
				$queried_object = get_queried_object();
				if ($queried_object){
					$postid = $queried_object->ID;
					if(isset($exclude[$postid]))
						$run = false;
				}
			}
		}

		if(has_filter('woobought-run')) {
			$run = apply_filters('woobought-run', $run);
		}

		wp_localize_script(MABEL_WOOBOUGHT_SLUG,'mabelrpnrun', (string)($run?1:0));
	}
	
	// Private Helpers
    private function getOption($id){
        $o = $this->options[$id];
        return (isset($o) && $o!=='')?$o:$this->defaults[$id];
    }
	
	private function t($key){
		return __($key,MABEL_WOOBOUGHT_SLUG);
	}
	
	// Merge options with defaults;
	private function mergeOptions(){
		return array_merge($this->defaults,$this->options);
	}
	
	private function valueOrTranslateDefault($key){
		return (empty($this->options[$key])? $this->t($this->defaults[$key]) : $this->options[$key]);
	}
}